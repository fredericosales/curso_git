# Título

## subtitulo

### tabela
| A | B | C |
|:-:|:-:|:-:|
| a | b | c |
| d | e | f |
| g | h | i |
| j | k | l | 
| m | n | o |
| p | q | r | 
| s | t | u |
| v | w | x |
| y | z | - |

	texto em destaque

Aqui uma [url](http://google.com)


### listas

* um
* dois
	* subitem a
	* subitem b
* tres

### python code
```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<fredericosales@engenharia.ufjf.br>
2018
"""

# main 
def main():
	pass


if __name__ == '__main__':
	main()
```

### c code

```c
/*
 * Frederico Sales
 * <fredericosales@engenharia.ufjf.br>
 * 2018
 */

// include
#include <iostream>

// namespace
using namespace std;

// main 
int main(int argc, char argv[]) {
	// code here
	cout << "here comes the pain" << endl
	return 0;
}
```

### java
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

### lista de tarefas

- [x] @mentions, #refs, [links](), **formating**,  and <del>tags</del>
- [x] __here__  _comes_ ___the___ _pain_
- [x] agora falta aprender direito
- [ ] a preguica consumindo meu dia

### inutilidades

~~this~~

#### emoji

:cold_sweat
:shit
:poop

### imagens

![Image of Yaktocat](https://octodex.github.com/images/yaktocat.png)

### referencias

#1
#2
#3